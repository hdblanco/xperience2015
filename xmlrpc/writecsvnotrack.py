import openerplib
import time

class Products():
    def update_using_csv(self, connection):
        product_model = connection.get_model('product.template')
        
        product_ids = product_model.search([])
        
        columns = ['id', 'list_price']
        lines = []
        lineswithtrack = []
        
        products = product_model.export_data(product_ids, ['id', 'name', 'list_price'])
        for product in products['datas']:
            lines.append([product[0], float(product[2])*1.1])
            lineswithtrack.append([product[0], float(product[2])*0.9])

        start = time.time()
        product_model.load(columns, lineswithtrack)
        middle = time.time()
        product_model.load(columns, lines, context={'tracking_disable': True})
        end = time.time()
        
        print middle - start
        print end - middle

if __name__ == '__main__':
    #Connect by xml-rpc
    connection = openerplib.get_connection(hostname="localhost", 
                                           port=8069, 
                                           database="xp2015", 
                                           login="admin", 
                                           password="admin", 
                                           protocol="jsonrpc",
                                           user_id=1)
                                           
    connection.check_login()
            
    prod = Products()
    prod.update_using_csv(connection)
