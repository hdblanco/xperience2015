import openerplib

class Products():
    def update_using_csv(self, connection):
        product_model = connection.get_model('product.product')
        
        product_ids = product_model.search([('name', 'ilike', 'ipad')])
        
        columns = ['id', 'list_price']
        lines = []
        
        products = product_model.export_data(product_ids, ['id', 'name', 'list_price'])
        for product in products['datas']:
            lines.append([product[0], float(product[2])*1.1])
        product_model.load(columns, lines)

if __name__ == '__main__':
    #Connect by xml-rpc
    connection = openerplib.get_connection(hostname="localhost", 
                                           port=8069, 
                                           database="xp2015", 
                                           login="admin", 
                                           password="admin", 
                                           protocol="jsonrpc",
                                           user_id=1)
                                           
    connection.check_login()
            
    prod = Products()
    prod.update_using_csv(connection)
