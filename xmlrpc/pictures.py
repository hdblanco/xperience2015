# -*- encoding: utf-8 -*-
import openerplib
import os
import sys
import traceback
import requests
import xml.etree.ElementTree as ET
import tempfile
import base64
import shutil
import datetime
import threading
import copy

class IceCat():
    max_connections = 3
    semaphore = threading.BoundedSemaphore(max_connections)
    
    def __init__(self, username, password):
        self.username = username
        self.password = password
            
    def get_itemdesc_ean(self, ean, lang="en"):
        url = "https://data.icecat.biz/xml_s3/xml_server3.cgi?ean_upc=%s;lang=%s;output=productxml" % (ean, lang)
        r = requests.get(url, auth=(self.username, self.password))
        if r.status_code == 200:
            art = ET.fromstring(r.text.encode('utf-8'))
            if 'ErrorMessage' in art.find("Product").attrib and art.find("Product").attrib['ErrorMessage']:
                return (None, art.find("Product").attrib['ErrorMessage'])
            return (art, "")
        return (None, unicode(r.status_code))
    
    def _get_image(self, remotefile, tmp_dir):
        if remotefile:
            try:
                localfile = tmp_dir.rstrip("/") + "/" + remotefile.split("/")[-1]
                response = requests.get(remotefile, stream=True)
                if response.ok:
                    with open(localfile, 'wb') as handle:
                        response.raw.decode_content = True
                        shutil.copyfileobj(response.raw, handle)
                    with open(localfile, "rb") as image_file:
                        encoded_string = base64.b64encode(image_file.read())
                        image_file.close()
                    os.remove(localfile)
                    return encoded_string
            except Exception, e:
                return False
        return False
        
    def _check_field(self, connection):
        model_model = connection.get_model('ir.model')
        field_model = connection.get_model('ir.model.fields')
        
        model_id = model_model.search([('model', '=', 'product.template')])
        field_id = field_model.create({
            'name': 'x_picturedate',
            'field_description': 'Picture import date',
            'model': 'product.template',
            'model_id': model_id[0],
            'ttype': 'datetime',
            'state': 'manual',
        })

    def set_picture_in_db(self, template_model, prod_id, img, tmp_dir):
        try:
            template_model.write([prod_id, ], {'image': self._get_image(img, tmp_dir), 'x_picturedate': datetime.datetime.now().isoformat()})
        except Exception, e:
            print e
        finally:
            self.semaphore.release()
            
    def get_descs(self, connection, tmp_dir):
        lst_thd = []
        
        template_model = connection.get_model('product.template')
        
        self._check_field(connection)

        product_ids = template_model.search([('barcode', '!=', False)]) #('x_picturedate', '=', False), 
        products = template_model.read(product_ids, ['barcode',])
        for product in products:
            desc, error = self.get_itemdesc_ean(product['barcode'], "en")
            if desc is not None:
                if desc.find("Product").attrib['HighPic'] is not None:
                    self.semaphore.acquire()
                    thd = threading.Thread(target=self.set_picture_in_db, args=(template_model, product['id'], desc.find("Product").attrib['HighPic'], tmp_dir))
                    thd.start()
                    lst_thd.append(thd)
                    
        for thd in lst_thd:
            thd.join()
            
if __name__ == '__main__':
    #Connect by xml-rpc
    connection = openerplib.get_connection(hostname="localhost", 
                                           port=8069, 
                                           database="xp2015", 
                                           login="admin", 
                                           password="admin", 
                                           protocol="jsonrpc",
                                           user_id=1)
                                           
    connection.check_login()
    
    tmp_dir = tempfile.mkdtemp()
    try:
        icecatuser = 'theawesomeuser'
        icecatpass = 'thesecretpassword'
        icecat = IceCat(icecatuser, icecatpass)
        icecat.get_descs(connection, tmp_dir)
    except Exception, e:
        tb = traceback.format_exc()
        log_model = connection.get_model('ir.logging')
        datadict={
            'name': "edi client - worker - icecat",
            'type': 'client',
            'dbname': 'madb',
            'level': 'Error',
            'message': "%s \r\n %s" % (unicode(e),tb),
            'path': "icecat",
            'func': "get_descs",
            'line': "0",
        }
        log_model.create(datadict)
        print tb
    finally:
        shutil.rmtree(tmp_dir)
