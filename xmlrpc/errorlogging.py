import openerplib
import traceback

class HelloWorld():
    def say_hello(self, connection):
        partner_model = connection.get_model('res.partner')
        try:
            partner_ids = partner_model.search([('name', 'ilike', 'fletcher')])
            partners = partner_model.read(partner_ids, ['name', 'parent_id'])
            for partner in partners:
                print partner
                res = "Hello %s" % partner['name']
                if partner['parent_id']:
                    res = "%s from %s" % (res, partner['parent_id'][1])
                print res
                print 1/0
                
        except Exception, e:
            tb = traceback.format_exc()
            log_model = connection.get_model('ir.logging')
            datadict = {
                'name': "say_hello",
                'type': 'client',
                'dbname': 'xp2015',
                'level': 'Error',
                'message': "%s \r\n %s" % (unicode(e), tb),
                'path': "HelloWorld",
                'func': "say_hello",
                'line': "0",
            }
            log_model.create(datadict)

if __name__ == '__main__':
    #Connect by xml-rpc
    connection = openerplib.get_connection(hostname="localhost", 
                                           port=8069, 
                                           database="xp2015", 
                                           login="admin", 
                                           password="admin", 
                                           protocol="jsonrpc",
                                           user_id=1)
                                           
    connection.check_login()
            
    imp = HelloWorld()
    imp.say_hello(connection)
    
